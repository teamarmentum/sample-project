<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage SP_Theme
 * @since SP Theme 1.0
 */

get_header(); ?>
<?php if($post->post_name=='about'){?>
<div class="jumbotron about_us">
  <div class="container">
    <h2>Our Company</h2>
    <h4 class="text-nor">sp_theme was founded on the principle of creating profitable partnerships.</h4>
    <div class="search_blk">
      <input type="text" name="name">
      <input type="submit" value="START INVESTMENT NOW">
    </div>
    <!-- // search_blk  --> 
  </div>
</div>
<?php }else{ ?>
<div class="jumbotron">
      <div class="container">
        <h2 class="border-title">
			<span><?php the_title(); ?></span>
		</h2>  
      </div>
    </div>
<?php } ?>
<div class="container">
<?php
if ( have_posts() ) :
while ( have_posts() ) : the_post();
 	the_content(); 
endwhile;
endif;
 ?> </div>
<?php get_footer(); ?>
