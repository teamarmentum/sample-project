<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage SP_Theme
 * @since SP Theme 1.0
 */
?> 

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<span class="sticky-post"><?php _e( 'Featured', 'sp_theme' ); ?></span>
		<?php endif; ?>

		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
        
        
	</header><!-- .entry-header -->
		<hr>
        <div class="content-post-all-meta">
          <ul>
            <li><i class="fa fa-clock-o"></i><?php echo get_the_date(); ?></li>
            <li><a href="#."><i class="fa fa-comment fa-flip-horizontal"></i><?php comments_number( 'No Comment', 'one response', '% responses' ); ?></a></li>
          </ul>
        </div>
        	<?php sp_theme_post_thumbnail(); ?>
		
        <div class="content-post-desc">
		<?php the_excerpt(); ?>
    </div>


	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'sp_theme' ),
				get_the_title()
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'sp_theme' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'sp_theme' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php sp_theme_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'sp_theme' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
