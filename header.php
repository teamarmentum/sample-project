<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage sp_theme
 * @since sp_theme 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
    
    
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" <?php  echo $user_ID;?>>

<!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
     
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
             <?php sp_theme_the_custom_logo(); ?>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
           <?php 

           // show different menu and layout if user logged in
     		   if(is_user_logged_in()){
    				 $current_user_id= get_current_user_id();
    				 $args=array("class"=>"img-circle");
    				 $alt="";
    				 $default="";
    				 $user_data=get_userdata( $current_user_id );

 				 wp_nav_menu( array(
							'menu' => 'Login user menu',
							'menu_class' => 'nav navbar-nav',
							'fallback_cb' => 'wp_page_menu',
							'depth' => 0,
							'theme_location' => 'primary-guest'
						 ) );
								?>
        <ul class="nav navbar-nav navbar-right">
             <li class="user"><a href="<?php echo site_url('profile/profile-info') ?>" >
	             Hi, <?php echo $user_data->user_firstname;?>
	         	 <img src="<?php echo user_img_url($current_user_id); ?>" class="img-circle"/>
            </li>
            <li class="signup">
            	<a href="<?php echo wp_logout_url(home_url('login/?act=logout')); ?>">Logout</a>
            </li>
          </ul>
        <?php }else{ 
 	       	   wp_nav_menu( array(
						'menu_class' => 'nav navbar-nav',
						'fallback_cb' => '',
						'theme_location' => 'primary'
					 ) );
				?>
        <ul class="nav navbar-nav navbar-right">
            <li class="login"><a href="<?php echo site_url('login') ?>">Login</a></li>
            <li class="signup"><a href="<?php echo site_url('register')?>">Sign up <span class="sr-only">(current)</span></a></li>
          </ul>
        <?php } ?>
          
        
          
        </div><!--/.nav-collapse -->
     
    </nav>
<!-- end Nav -->




	

