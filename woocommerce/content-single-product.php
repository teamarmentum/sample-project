<?php

/**

 * The template for displaying product content in the single-product.php template

 *

 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.

 *

 * HOWEVER, on occasion WooCommerce will need to update template files and you

 * (the theme developer) will need to copy the new files to your theme to

 * maintain compatibility. We try to do this as little as possible, but it does

 * happen. When this occurs the version of the template file will be bumped and

 * the readme will list any important changes.

 *

 * @see 	    https://docs.woocommerce.com/document/template-structure/

 * @author 		WooThemes

 * @package 	WooCommerce/Templates

 * @version     1.6.4

 */

global $product;



if ( ! defined( 'ABSPATH' ) ) {

	exit; // Exit if accessed directly

}



?>
<?php

	/**

	 * woocommerce_before_single_product hook.

	 *

	 * @hooked wc_print_notices - 10

	 */

	 do_action( 'woocommerce_before_single_product' );



	 if ( post_password_required() ) {

	 	echo get_the_password_form();

	 	return;

	 }

	 

//Attributes

$user_id=get_current_user_id();

$no_properties= $product->get_attribute( 'no-of-properties' );

$property_type= $product->get_attribute( 'property-type' ); 

$offering_size= $product->get_attribute( 'offering-size' );

$minimum_investment = $product->get_attribute( 'minimum-investment' );

$irr_from = $product->get_attribute( 'irr-from' );

$irr_to = $product->get_attribute( 'irr-to' );

$address = $product->get_attribute( 'address' );

$downloads = $product->get_files();

$term_list = wp_get_post_terms($product->id,'product_cat',array('fields'=>'ids'));

$cat_id = (int)$term_list[0];

$cat=get_term($cat_id, 'product_cat');

$cat_slug=$cat->slug;
$ccr=$product->get_attribute( 'ccr' );
$ccr_to=$product->get_attribute( 'ccr_to' );
$ccr_from=$product->get_attribute( 'ccr_from');
$property_term=$product->get_attribute( 'property-term');
$est_fmv=$product->get_attribute( 'estimated-fair-market-value');
$esti_cash=($minimum_investment*$ccr)/4;


?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php

		/**

		 * woocommerce_before_single_product_summary hook.

		 *

		 * @hooked woocommerce_show_product_sale_flash - 10

		 * @hooked woocommerce_show_product_images - 20

		 */

		do_action( 'woocommerce_before_single_product_summary' );
		$sub_property_disp=sub_property_display($product->id);
	?>
<?php if($sub_property_disp!=""){?>
<div class="bg_gray full_width brdrBdark">
  <div class="container">
    <div class="row">
      <div class="col-md-12 content_16 similar_properties_blk">
        <?php echo $sub_property_disp;?>
        
      </div>
    </div>
    <!-- end row --> 
  </div>
</div>
<?php } ?>
<div class="container properties_det content_16 mrgnB30 mrgnT30">
  <div class="row">
    <div class="col-md-7 col-sm-8 col-xs-12 summary_properties">
      <h3>Summary</h3>
      <?php 
      $content = get_post($product->id)->post_content;
      $content = apply_filters('the_content',$content);
      echo $content; ?>
      <?php /*<div class="clearfix"> </div>
      <h3>Portfolio Details</h3>
      <ul class="list_n protfolio_det">
        <li>
          <div class="lb_title">Number of properties</div>
          <div class="lb_value">
            <?php  echo $no_properties;?>
          </div>
        </li>
        <li>
          <div class="lb_title">Property Type</div>
          <div class="lb_value">
            <?php  echo $property_type;?>
          </div>
        </li>
        <li>
          <div class="lb_title">Property Summary Document</div>
          <div class="lb_value"><?php echo get_file_by_name($downloads,'Property Summery Document');?> </div>
        </li>
      </ul>
      */  ?>
      <div class="clearfix"> </div>
      <?php 
       $excerpt = get_post($product->id)->post_excerpt;
      $excerpt = apply_filters('the_content',$excerpt);
      //echo $excerpt;
      //echo the_excerpt();?>
      <div class="clearfix"> </div>
      <?php echo download_files($downloads,'Property Summery Document');?> </div>
    <div class="col-md-offset-1 col-md-4 col-sm-4 col-xs-12">
      <div class="key_highlights">
        <h2>Key Highlights</h2>
        <div class="progress_block text-center">
          <?php
            $rating = $product->get_attribute( 'rating' );
            if($rating >33){
          ?>
          <div class="barWrapper">
            <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $rating; ?>" aria-valuemin="0" aria-valuemax="100" > <span  class="popOver" data-toggle="tooltip" data-placement="top" title="<?php echo $rating; ?>%"> </span> </div>
            </div>
          </div>
          <?php } ?>
          
          <!-- end progress block -->
          
          <ul class="list_n protfolio_det">
            <li>
              <div class="lb_title">Offering Size </div>
              <div class="lb_value">$<?php  echo ($offering_size) ? number_format($offering_size) : '-';?>
              </div>
            </li>
            <li>
              <div class="lb_title">Number of Properties </div>
              <div class="lb_value"><?php  echo $no_properties;?>
              </div>
            </li>
            <li>
              <div class="lb_title">Security Type</div>
              <div class="lb_value">
                <?php  echo $property_type;?>
              </div>
            </li>
            <li>
              <div class="lb_title">Minimum Investment</div>
              <div class="lb_value">$<?php  echo ($minimum_investment) ? number_format($minimum_investment) : '-';?>
              </div>
            </li>
            <li>
              <div class="lb_title">Projected IRR</div>
              <div class="lb_value">
                <?php  echo $irr_from;?>% -
                <?php  echo $irr_to;?>%</div>
            </li>
            <li>
              <div class="lb_title">Projected Cash on Cash Return</div>
              <div class="lb_value">
                <?php  echo $ccr_from;?>% -
                <?php  echo $ccr_to;?>%</div>
            </li>
            <li>
              <div class="lb_title">Term</div>
              <div class="lb_value"><?php echo $property_term;?></div>
            </li>
            <li>
              <div class="lb_title">Estimated Fair Market Value</div>
              <div class="lb_value">$<?php echo ($est_fmv) ? number_format($est_fmv) : '-';?></div>
            </li>
          </ul>
          <div class="clearfix"> </div>
          <?php 

		        if($cat_slug=='current-opportunities')
		        {
					$check_inv=check_invested_prop($user_id,$product->id);
					if(count($check_inv)>0)
					{		
					 if($check_inv->post_status=='wc-completed'){
					   $payment_message = "Already Invested";
					    ?>
          <div class="submit_btn form text-center mrgnT15 mrgnB30"> <a href="javascript:void(0);" class="submit btn" ><?php echo $payment_message; ?></a> </div>
					   <?php
					 }else if($check_inv->post_status !='wc-pending'){
					   $payment_message = "Invest Now";
					    ?>
           <div class="submit_btn form text-center mrgnT15 mrgnB30"> <a href="javascript:void(0);" class="submit btn" onclick="invest('invbtn','<?php echo $product->id; ?>','<?php echo get_site_url();?>');" id="invbtn">Invest Now</a> </div>
					   <?php
					 }else{
					   $payment_message = "Pending Approval";
					   ?>
          <div class="submit_btn form text-center mrgnT15 mrgnB30"> <a href="javascript:void(0);" class="submit btn" ><?php echo $payment_message; ?></a> </div>
					   <?php
					 }
					
		        ?>
          <?php }else{ ?>
          
          
               <div class="submit_btn form text-center mrgnT15 mrgnB30"> <a href="javascript:void(0);" class="submit btn" onclick="invest('invbtn','<?php echo $product->id; ?>','<?php echo get_site_url();?>');" id="invbtn">Invest Now</a> </div>
          <?php } ?>
          
          <?php }elseif($cat_slug=='upcoming'){
			  
					$check_resv=check_reserved_prop($user_id,$product->id);
					
					if(count($check_resv)==0)
					{
			   ?>
          <div class="submit_btn form text-center mrgnT15 mrgnB30"> <a href="javascript:void(0);" class="submit btn" id="reservspot">Reserve My Spot</a> </div>
          
          <?php }else{ ?>
           <div class="submit_btn form text-center mrgnT15 mrgnB30"> <a href="javascript:void(0);" class="submit btn">Reserved</a> </div>
          
          <?php } ?>
          <div class="modal fade bs-example-modal-lg" id="myModalReserv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="step_content">
                      <div class="step_content_area">
                        <h3>Reserve My Spot</h3>
                        <div id="reserve_response"></div>
                        <form method="post" id="reserv_spot_from" class="comment-form form" novalidate="novalidate">
                          <input type="hidden" name="serv_root" id="serv_root" value="<?php echo get_site_url();?>">
                              <input type="hidden" name="minvsest" id="minvsest" value="<?php echo $minimum_investment;?>">
                              <input type="hidden" name="pro_id" id="pro_id" value="<?php echo $product->id;?>">
                          
                          
                          <div class="min_investment"> <span>Min Investment:   $<?php echo $minimum_investment;?></span> </div>
                          <div class="comment-investment-amount">
                            <label for="author"><strong>Investment Amount</strong></label>
                            <div class="input-group"> <span class="input-group-addon">$</span>
                              <input id="inv_amount" name="inv_amount" required="required" title="Please Enter Investment Amount" type="text" value="" size="30" aria-required="true">
                            </div>
                          </div>
                          <p class="form-estimated-quaterly text-center content_16"> Estimated Quaterly Cash Flow: <br>
                            $<?php echo $esti_cash;?> </p>
                          <div class="clearfix"> </div>
                          <div class="submit_btn form text-center mrgnT15">
                            <button class="submit btn" type="submit">Reserve</button>
                          </div>
                        </form>
                      </div>
                      
                      <!-- // end contact_question --> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
          <div class="clearfix"> </div>
        </div>
        
        <!-- end key_highlights -->
        
        <div class="contact_question">
          <h3>Question about this specific offering?</h3>
          <h4>Please email us at <a href="mailto:info@sp_theme.com">info@sp_theme.com <br>
            Or Call us <span>317-440-1515</span></a></h4>
          <a href="mailto:info@sp_theme.com"> </a></div>
        <a href="mailto:info@sp_theme.com"> 
        
        <!-- // end contact_question --> 
        
        </a></div>
      <a href="mailto:info@sp_theme.com"> </a></div>
    <div class="summary entry-summary">
      <?php

			/**

			 * woocommerce_single_product_summary hook.

			 *

			 * @hooked woocommerce_template_single_title - 5

			 * @hooked woocommerce_template_single_rating - 10

			 * @hooked woocommerce_template_single_price - 10

			 * @hooked woocommerce_template_single_excerpt - 20

			 * @hooked woocommerce_template_single_add_to_cart - 30

			 * @hooked woocommerce_template_single_meta - 40

			 * @hooked woocommerce_template_single_sharing - 50

			 */

			//do_action( 'woocommerce_single_product_summary' );

		?>
    </div>
    
    <!-- .summary --> 
    
  </div>
  <?php

		/**

		 * woocommerce_after_single_product_summary hook.

		 *

		 * @hooked woocommerce_output_product_data_tabs - 10

		 * @hooked woocommerce_upsell_display - 15

		 * @hooked woocommerce_output_related_products - 20

		 */

		//do_action( 'woocommerce_after_single_product_summary' );

	?>
  <meta itemprop="url" content="<?php the_permalink(); ?>" />
</div>

<!-- #product-<?php the_ID(); ?> -->

<?php //do_action( 'woocommerce_after_single_product' ); ?>
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content"> </div>
          </div>
        </div>