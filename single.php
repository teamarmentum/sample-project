<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage SP_Theme
 * @since SP Theme 1.0
 */

get_header(); ?>
<div class="jumbotron blogpage">
      <div class="container">
        <h2 class="border-title">
			<span>Blog</span>
		</h2>  
      </div>
    </div>

<div class="container"> 
 <div class="row">
    <div class="col-md-12 content_16 post-listing">

    



		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
			get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}


			// End of the loop.
		endwhile;
		?>

	

	<?php get_sidebar( 'content-bottom' ); ?>

</div>
 <!--<div class="col-md-4 content_16 post-listing">
<?php // get_sidebar(); ?>
</div>-->
</div>
</div>
<?php get_footer(); ?>
