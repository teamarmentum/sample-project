## Reference

At Armentum, we build digital products that generate revenue. Our team has worked with large institutions such as the Scripps Institute to build their Gene Annotation Portal to fast-growing startups such as sp_theme to build their crowdfunding marketplace for real-estate portfolios. Our team loves contributing to the open-source and also patterning with other agencies to build award-winning digital experiences.

## Installation

Copy this project folder to wordpress themes folder
