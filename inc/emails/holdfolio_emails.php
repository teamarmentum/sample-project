<?php

/*
* Email Tamplates
*/
include_once('registration.php');
include_once('new_investment_request.php');
include_once('new_reserve_request.php');
include_once('update_reserve_request.php');
include_once('portfolio_completed_notification.php');
include_once('portfolio_cancelled_notification.php');
include_once('updated_date_change.php');
include_once('uploaded_documents.php');