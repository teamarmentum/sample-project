<?php

/*
* Email Tamplate for order canceled notification
*/
function woocommerce_order_status_cancelled_notification_send($order_id){
    $object =  wc_get_order( $order_id );
    $product_id = $object->post->post_parent;
    $email = get_post_meta($order_id,'_billing_email',true);
  
  
    $customer_id = get_post_meta($order_id,'_customer_user',true);
    $firstname = get_user_meta($customer_id,'first_name',true);
    $product_name = get_the_title($product_id);
    $message = "Hi {$firstname},<br />

Your investment request in {$product_name} has been canceled. Please reply to

this email if you have any questions.<br /><br />

Sincerely,<br />

The sp_theme Team";

add_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );

$subject = ot('investment_request_canceled_subject');
		$subject = str_replace('{firstname}',$firstname,$subject);
		$subject = str_replace('{product_name}',$product_name,$subject);
		
		
		$content = ot('investment_request_canceled_content');
		$content = str_replace('{firstname}',$firstname,$content);
		$content = str_replace('{product_name}',$product_name,$content);
		
		
wp_mail( $email, $subject, $content );
remove_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );
}
add_action( 'woocommerce_order_status_pending_to_cancelled_notification', 'woocommerce_order_status_cancelled_notification_send' );
add_action( 'woocommerce_order_status_on-hold_to_cancelled_notification', 'woocommerce_order_status_cancelled_notification_send' );
