<?php

/*
* Email Tamplate for new registraion notification
*/
function sp_theme_after_registration_send_notification($email,$userdata){
 

$body = "Hi {$userdata['first_name']},<br />

Thank you for registering with our unique company. This email is to confirm that

you have successfully created your account. Our company prides itself in the

profitable partnerships that we create, and we’re excited that you have shown

interest in our model.<br /><br />

Sincerely,<br />

The sp_theme Team";  
add_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );

$subject = ot('user_register_subject');
		$subject = str_replace('{firstname}',$userdata['first_name'],$subject);
		$subject = str_replace('{product_name}',$product_name,$subject);
		
		
		$content = ot('user_register_content');
		$content = str_replace('{firstname}',$userdata['first_name'],$content);
		$content = str_replace('{product_name}',$product_name,$content);
wp_mail( $email, $subject, $content );

remove_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );
}
add_action('sp_theme_after_registration','sp_theme_after_registration_send_notification',10,2);