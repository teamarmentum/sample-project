<?php

/*
* Email Tamplate for date change notification
*/
function send_updated_date_email_notification_to_users( $post_id, $post, $update ) {

if($post->post_type=='shop_order'){
    //$value = 'updated_date';
    //$value = 'earnings_to_date';
    
        $value = ot('earnings_to_date_change_field');
    $old_updated_date = get_post_meta($post_id,$value,true);
    $new_date = $_POST[$value];
    
    // check old date is different from now.
    if($old_updated_date != $new_date){
        $object =  wc_get_order( $post_id );
         $email = get_post_meta($post_id,'_billing_email',true);
         $customer_id = get_post_meta($post_id,'_customer_user',true);
            $firstname = get_user_meta($customer_id,'first_name',true);
            
             $product_id = $object->post->post_parent;
             $product_name = get_the_title($product_id);
            
                $body = "Hi {$firstname},<br />
        
        Your Earnings to Date has been updated for {$product_name}. Please login to
        
        your online dashboard to view more details.<br /><br />
        
        Sincerely,<br />
        
        The sp_theme Team";  
        
        
        add_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );
        
        $subject = ot('earnings_to_date_change_subject');
		$subject = str_replace('{firstname}',$firstname,$subject);
		$subject = str_replace('{product_name}',$product_name,$subject);
		
		
		$content = ot('earnings_to_date_change_content');
		$content = str_replace('{firstname}',$firstname,$content);
		$content = str_replace('{product_name}',$product_name,$content);
		
		
        wp_mail( $email, $subject, $content );
        
        remove_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );
    }
}
   
}
add_action( 'save_post', 'send_updated_date_email_notification_to_users', 1, 3 );
