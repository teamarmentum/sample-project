<?php

/*
* Email Tamplate for new investment request notification
*/
function sp_theme_after_new_investment_request_send_notification($order_id,$session){
   
add_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );

	global $wpdb,$current_user;
	get_currentuserinfo();
	
	
		$pro_id=$session['invest']['pro_id'];
	$project_name=get_the_title($pro_id);
		$investing_as=$session['invest']['inv_as'];
		$name_inv_as=$session['invest']['name_inv_as'];
		$amount_invested=$session['invest']['inv_amt'];
		$user_name=$current_user->first_name.' '.$current_user->last_name;
		$to = get_bloginfo('admin_email');
		
		$subject = "New Investment Request by {$user_name}";
		
		$mail_body="<p>Dear Admin,</p>";
		$mail_body.= "New Investment Request by {$user_name}";
		
		$mail_body.="<h4>Investment Details</h4>";
		$mail_body.="<p><strong>Project name<strong>: {$project_name}</p>";
		$mail_body.="<p><strong>First Name<strong>: {$current_user->first_name}</p>";
		$mail_body.="<p><strong>Last Name<strong>: {$current_user->last_name}</p>";
		$mail_body.="<p><strong>Email Entered<strong>: {$session['invest']['step_email']}</p>";
		$mail_body.="<p><strong>Investing As <strong>: {$investing_as}</p>";
		if($investing_as=='An LLC, Corporation, or Partnership')
		{
		$mail_body.="<p><strong>Name of LLC/Corp/Partnership' <strong>: {$name_inv_as}</p>";
		}
		elseif($investing_as=='A Trust')
		{
		$mail_body.="<p><strong>Name of Trust'<strong>: {$name_inv_as}</p>";
		}
		$mail_body.="<p><strong>Amount Investing'<strong>: {$amount_invested}</p>";
		$message = $mail_body;
		//$headers  = 'MIME-Version: 1.0' . "\r\n";
		//$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		//$headers .= 'From: sp_theme <info@sp_theme.com>' . "\r\n";
		//$flag = mail($to, $subject, $message, $headers,'-finfo@sp_theme.com');
		
// to admin		
wp_mail( $to, $subject, $message );



$object =  wc_get_order( $order_id );
    $product_id = $object->post->post_parent;
    $email = get_post_meta($order_id,'_billing_email',true);
  
  
    $customer_id = get_post_meta($order_id,'_customer_user',true);
    $firstname = get_user_meta($customer_id,'first_name',true);
    $product_name = get_the_title($product_id);
    
    
    $message2 = "Hi {$firstname},<br />

Thank you for your investment request in {$product_name}. We will be sending

any documents that need to be signed to you via CudaSign. Please keep a

lookout for these documents and if you haven’t received them within 24 hours,

please reply to this email.<br /><br />

Sincerely,<br />

The sp_theme Team";
// to user


		$subject = ot('investment_request_customer_subject');
		$subject = str_replace('{firstname}',$firstname,$subject);
		$subject = str_replace('{product_name}',$product_name,$subject);
		
		
		$content = ot('investment_request_customer_content');
		$content = str_replace('{firstname}',$firstname,$content);
		$content = str_replace('{product_name}',$product_name,$content);
		
		
wp_mail( $email, $subject, $content );


		

remove_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );
}
add_action('sp_theme_after_new_investment_request','sp_theme_after_new_investment_request_send_notification',10,2);