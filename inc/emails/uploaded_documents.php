<?php

/*
* Email Tamplate for document updated notification
*/
function send_uploaded_documents_email_notification_to_users( $post_id, $post, $update ) {
    	global $wpdb;
			$t_posts = $wpdb->posts;
			$t_order_items = $wpdb->prefix . "woocommerce_order_items";  
			$t_order_itemmeta = $wpdb->prefix . "woocommerce_order_itemmeta";
			
	
if($post->post_type=='product'){
    $old_uploaded_docs = get_post_meta($post_id,'_downloadable_files',true);
    $count = count($old_uploaded_docs);
    
    
    $new_documents_count = count($_REQUEST['_wc_file_names']);
    if($new_documents_count > $count){
        	$sql="SELECT $t_order_items.order_id FROM $t_order_items 
		LEFT JOIN $t_order_itemmeta on $t_order_itemmeta.order_item_id=$t_order_items.order_item_id
		WHERE $t_order_items.order_item_type='line_item' AND $t_order_itemmeta.meta_key='_product_id' AND $t_order_itemmeta.meta_value IN ($post_id)";
		
		$all_posts = $wpdb->get_results($sql, ARRAY_A);
		
			$all_email_ids = array();
		foreach($all_posts as $ap){
		    $order_id = $ap['order_id'];
		    $new_order = new WC_Order($order_id);
		   if($new_order->post_status=='wc-pending' || $new_order->post_status=='wc-completed'){
         $customer_id = get_post_meta($order_id,'_customer_user',true);
            $firstname = get_user_meta($customer_id,'first_name',true);
		    $email = $new_order->billing_email;
		    
             $product_name = get_the_title($post_id);
		    
		     $body = "Hi {$firstname},<br />

A new document has been published for {$product_name}. Please login to your

online dashboard to view more details.<br /><br />

Sincerely,<br />

The sp_theme Team";  
        
        
        add_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );
        
        $subject = ot('new_document_subject');
		$subject = str_replace('{firstname}',$firstname,$subject);
		$subject = str_replace('{product_name}',$product_name,$subject);
		
		
		$content = ot('new_document_content');
		$content = str_replace('{firstname}',$firstname,$content);
		$content = str_replace('{product_name}',$product_name,$content);
		
        wp_mail( $email, $subject, $content );
        
        remove_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );
        
		   }
		}
    }
    
    
}
   
}
add_action( 'save_post', 'send_uploaded_documents_email_notification_to_users', 1, 3 );
