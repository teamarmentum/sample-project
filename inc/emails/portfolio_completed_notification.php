<?php

/*
* Email Tamplate for new investment paid notification
*/
function woocommerce_order_status_completed_notification_send($order_id){
   
    $object =  wc_get_order( $order_id );
    add_post_meta($order_id,'_order_confiremed_date',date('Y-m-d H:i:s'));
    $product_id = $object->post->post_parent;
    $email = get_post_meta($order_id,'_billing_email',true);
  
  
    $customer_id = get_post_meta($order_id,'_customer_user',true);
    
    $attachemnt_id = get_post_meta($order_id,'welcome_pdf',true);
    $attachment_url = get_attached_file($attachemnt_id);
    $firstname = get_user_meta($customer_id,'first_name',true);
    $product_name = get_the_title($product_id);
    $message = "Hi {$firstname},<br />

Your investment request in {$product_name} has been confirmed. Please reply to

this email if you have any questions.<br /><br />

Sincerely,<br />

The sp_theme Team";

$message2 = "Hi {$firstname},<br />

Congratulations on your successful investment in {$product_name}! We have

attached a welcome letter that will clearly detail what you can expect going

forward.<br /><br />

All the best,<br />

The sp_theme Team";

add_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );
//wp_mail( $email, 'Your Investment Request has Been Confirmed', $message );
$attachments = array($attachment_url);

$subject = ot('investment_request_approved_subject');
		$subject = str_replace('{firstname}',$firstname,$subject);
		$subject = str_replace('{product_name}',$product_name,$subject);
		
		
		$content = ot('investment_request_approved_content');
		$content = str_replace('{firstname}',$firstname,$content);
		$content = str_replace('{product_name}',$product_name,$content);
		
wp_mail( $email, $subject, $content,'',$attachments );
remove_filter( 'wp_mail_content_type', 'sp_theme_set_html_mail_content_type' );
}
add_action('woocommerce_order_status_completed_notification','woocommerce_order_status_completed_notification_send');
