<?php

/**
	 * add an ajax function for loading investment for both loggedin and logged out users
	 *
	 *
	 * @param string $post_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 * }
	 * @return invested array.
*/
add_action('wp_ajax_nopriv_loadInvestment', 'load_investment'); 
add_action('wp_ajax_loadInvestment', 'load_investment');
function load_investment(){
	global $wpdb;
	$id_product=$_POST['pid'];

	$term_list = wp_get_post_terms($id_product,'product_cat',array('fields'=>'ids'));
	$cat_id = (int)$term_list[0];
	$cat=get_term($cat_id, 'product_cat');
	$category=get_term_link ($cat_id, 'product_cat');
	$cat_slug=$cat->slug;

	// check user logged in
	if(is_user_logged_in())
	{

	if($cat_slug=='current-opportunities')
	{
		$_SESSION['invest']=array("user"=>get_current_user_id(),"pro_id"=>$id_product,"step1"=>"incomplete","step2"=>"incomplete","step3"=>"incomplete");
		
		
		$res=array("result"=>"ok","redirect"=>get_site_url().'/investment-steps/');
	}
	else
	{
		$res=array("result"=>"denied","message"=>"Invalid Invest response");
	}
	}
	else
	{
		$res=array("result"=>"denied","message"=>"Invalid Invest response");
	}
	$res_array=json_encode($res);
	echo $res_array;

	die();
}


/**
	 * add an ajax function for loading next investment for both loggedin and logged out users
	 *
	 *
	 * @param string $post_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 * }
	 * @return invested array.
*/
add_action('wp_ajax_nopriv_loadNext', 'load_next'); 
add_action('wp_ajax_loadNext', 'load_next');
function load_next()
{
	global $wpdb;
	$s1=$_POST['s1'];
	$s2=$_POST['s2'];
	if($s1=='step1')
	{
		$_SESSION['invest']['step1']='completed';
	}
	

	
}

/**
	 * add an ajax function for downloadbale files for both loggedin and logged out users
	 *
	 *
	 * @param string $post_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 * }
	 * @return url.
*/
add_action('wp_ajax_nopriv_downloadFile', 'download_step1'); 
add_action('wp_ajax_downloadFile', 'download_step1');

function download_step1()
{
		
		
		$sr=$_POST['sr'];
		$file=$_POST['file'];
		$file_name=$_POST['file_name'];
		$_SESSION['invest']["download"][$sr]=$file;
		$_SESSION['invest']["download_name"][$sr]=$file_name;
		$url_file=get_site_url().'/download_file.php?path='.$file;
		echo $url_file;
		die();
}


/**
	 * add an ajax function for invest step 2
	 *
	 *
	 * @param string $post_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 * }
	 * @return ok and err if fails
*/
add_action('wp_ajax_nopriv_loadNextset2', 'load_next_step2'); 
add_action('wp_ajax_loadNextset2', 'load_next_step2');

function load_next_step2()
{
	global $wpdb;
	$inv_as=$_POST['inv_as'];
	$name_inv_as=$_POST['name_inv_as'];
	$inv_amt=$_POST['inv_amt'];
	$ccr_value=$_POST['ccr_value'];
	$total_estimated_value=$_POST['total_estimated_value'];
	if(is_numeric($inv_amt) && $inv_amt>0)
	{
		$pro_id=$_SESSION['invest']['pro_id'];
		$product = get_product( $pro_id );
		$min_investement=$product->get_attribute( 'minimum-investment' );
  		$ccr=$product->get_attribute( 'ccr' );
		$esti_cash=($inv_amt*$ccr)/4;
		
		$_SESSION['invest']['inv_amt']=$inv_amt;
		$_SESSION['invest']['name_inv_as']=$name_inv_as;
		$_SESSION['invest']['inv_as']=$inv_as;
		$_SESSION['invest']['esti_qt_cash']=$esti_cash;
		$_SESSION['invest']['step2']="completed";
		$_SESSION['invest']['ccr_value']=$ccr_value;
		$_SESSION['invest']['total_estimated_value']=$total_estimated_value;
		
		echo "ok";
		die();
	}
	else
	{
		echo "err";
		die();
	}
	
	
}



/**
	 * add an ajax function for invest step 3
	 *
	 *
	 * @param string $post_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 * }
	 * @return ok and err if fails
*/
add_action('wp_ajax_nopriv_loadNextset3', 'load_next_step3'); 
add_action('wp_ajax_loadNextset3', 'load_next_step3');

function load_next_step3()
{
	
	global $wpdb,$current_user;
	get_currentuserinfo();

	$step_email=$_POST['step_email'];
	
	if($step_email!="")
	{
		$_SESSION['invest']['step_email']=$step_email;
		$_SESSION['invest']['step3']="completed";
		
		$pro_id=$_SESSION['invest']['pro_id'];
		$product = get_product( $pro_id );
		$min_investement=$product->get_attribute( 'minimum-investment' );
  		//$ccr=$product->get_attribute( 'ccr' );
		//$esti_cash=($_SESSION['invest']['inv_amt']*$ccr)/4;
		$esti_cash = $_SESSION['invest']['total_estimated_value'];
		
		 $download_files=$_SESSION['invest']['download'];
		 $download_name=$_SESSION['invest']['download_name'];
		
		
    create_invest_order();
		
		//Email To Admin
		
	
	
		
		unset($_SESSION['invest']);
		
		echo "ok";
		die();
	}
	else
	{
		echo "err";
		die();
	}
	
	
}


/**
	 * add an ajax function for load reserved sopt
	 *
	 *
	 * @param string $post_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 * }
	 * @return ok and err if fails
*/
add_action('wp_ajax_nopriv_loadRevSpot', 'rev_spot'); 
add_action('wp_ajax_loadRevSpot', 'rev_spot');


function rev_spot()
{
	global $wpdb,$current_user;
	get_currentuserinfo();
	
	
	
	$inv_as=$_POST['inv_as'];
	$name_inv_as=$_POST['name_inv_as'];
	$inv_amt=$_POST['inv_amt'];
	$pro_id=$_POST['pro_id'];
	$product = get_product( $pro_id );
	$min_investement=$product->get_attribute('minimum-investment' );
  	$ccr=$product->get_attribute( 'ccr' );
	$esti_cash=($inv_amt*$ccr)/4;
	
	 $wpdb->insert( 'wp_reserv_spot', array(
        'user_id' => get_current_user_id(), 
        'pro_id' => $pro_id,
        'inv_amt' =>$inv_amt, 
        'inv_as' => $inv_as,
        'name_inv_as' => $name_inv_as, 
        'minimum_inv' => $min_investement,
        'ccr' => $ccr,
        'added_on' => time(),
        'ip' =>  $_SERVER["REMOTE_ADDR"] 
        ),
        array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') 
    );
	
		$project_name=get_the_title($pro_id);
		$user_name=$current_user->user_firstname.' '.$current_user->user_lastname;
		
		$to ='tilakaman@gmail.com';
		
		$subject = "New Spot Reservation Request by {$user_name}";
		
		$mail_body="<p>Dear Admin,</p><br>";
		$mail_body.= "New Spot Reservation Request by {$user_name}";
		
		$mail_body.="<h4>Investment Details</h4>";
		$mail_body.="<p><strong>Project name<strong>: {$project_name}</p>";
		$mail_body.="<p><strong>Investing As <strong>:".$inv_as."</p>";
		if($investing_as=='An LLC, Corporation, or Partnership')
		{
		$mail_body.="<p><strong>Name of LLC/Corp/Partnership' <strong>:".$name_inv_as."</p>";
		}
		elseif($investing_as=='A Trust')
		{
		$mail_body.="<p><strong>Name of Trust'<strong>:".$name_inv_as."</p>";
		}
		$mail_body.="<p><strong>Amount Investing'<strong>: $".$inv_amt."</p>";
		
		$message = $mail_body;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: sp_theme <info@sp_theme.com>' . "\r\n";
		$flag = mail($to, $subject, $message, $headers,'-finfo@sp_theme.com');
		$user_email=$current_user->user_email;
		
		$user_to=$user_email;
		$user_subject = "Spot Reservation";	
		$mail_body_user="<p>Dear {$user_name},</p><br>";
		$mail_body_user.="<h4>Thank you for your spot reservation on {$project_name}</h4>";
		
		$message = $mail_body_user;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: sp_theme <info@sp_theme.com>' . "\r\n";
		$flag = mail($user_to, $user_subject, $message, $headers,'-finfo@sp_theme.com');
		echo  "ok";
		exit;
	

}
