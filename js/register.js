/*
	Registeration
*/   	
var Signup = function() 
{
	"use strict";
	// Handle Signup Form
	var handleSignupForm = function() {
	$().ready(function() {
	// validate the comment form when it is submitted
	var validator =$("#regiserForm").validate({
	rules: {
			fname: {required: true},
			lname: {required: true},
			username: {required: true},
			email: {required: true, email:true},
			phone: {required: true, number: true, min:1},
			password: {required: true},
			confirm_password: 
			{
				 required: true,	
				 equalTo: "#password"
			},
			area: {required: true},
			invest:{required: true,  number: true, min:1},
			remember:{required: true}
		},
		
		messages: 
		{
		fname:{required: "Please Enter First Name"},
		lname:{required: "Please Enter Last Name"},
		username: {required: "Please Enter Username"},
		email:{required: "Please Enter Email",email:"Please Enter Valid Email"},
		phone: {required: "	Please Enter valid Phone Number"},
		password: {required: "Please Enter Password"},
		confirm_password:{
			required: "Please Enter Confirm Password",
			equalTo: "Please enter the same Password again"
		 },
		area: {required: "Please Select a Current Residency"},
		invest: {required: "Please Enter Investment Amount"},
		remember: {required: "Accept Our Terms And conditions "},
	
        },
        onfocusout:function(element){
         if (element.tagName === "TEXTAREA" || (element.tagName === "INPUT" && element.type !== "password")) {
        element.value = $.trim(element.value);
    }
    if (!this.checkable(element) && (element.name in this.submitted || !this.optional(element))) {
        this.element(element);
    }
        	enable_submit_button(element);
        },
        onclick:function(element){
        		// Click on selects, radiobuttons and checkboxes
					if ( element.name in this.submitted ) {
						this.element( element );

					// Or option elements, check parent select in that case
					} else if ( element.parentNode.name in this.submitted ) {
						this.element( element.parentNode );
					}
		        		enable_submit_button(element);
		        }
		   });
	   
	   function enable_submit_button(elem){
		//console.log(elem);
		//console.log(validator.numberOfInvalids());
	}
   
});


}
			
				return {
					init: function() {
						handleSignupForm(); // initial setup for comment form
					}
				}
			}();
				
				$(document).ready(function() {
					  $('#invest').keypress(function (event) {
						  return isNumber(event, this)
						});
					 $('[name="phone"]').keypress(function (event) {
						  return isNumber(event, this)
						});
					
					Signup.init();
				});
				
		