<?php 

/**
Template Name: Login
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage SP_Theme
 * @since SP Theme 1.0
 */
global $user_login;
get_header();

?>
<style>
.login-username label {
	display: none;
}
.login-password label {
	
	display: none;
}
.front_login #login_error {
      border-left-color: #dc3232;
    margin-bottom: 20px;
    border: 1px solid #efefef;
}
.front_login #login_error, .front_login .message {
    border-left: 4px solid #dc3232;
    padding: 12px;
    margin-left: 0;
    background-color: #fff;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}

.front_login #login_success
{
    border: 1px solid #efefef;
	  border-left: 4px solid #4CAF50;
    padding: 12px;
    margin-left: 0;
    background-color: #fff;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
	 margin-bottom: 20px;
}
.form label.error {
    color: red;
    font-weight: 400;
    margin-left: 10px;
}
</style>
<div class="jumbotron blogpage ">
  <div class="container">
    <h2 class="border-title"> <span>Login</span> </h2>
  </div>
</div>
<div class="container mrgnT30 login_page">
  <?php 
	        // In case of a login error.
   if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) : ?>
  <div class="aa_error">
    <p>
      <?php _e( 'FAILED: Try again!', 'AA' ); ?>
    </p>
  </div>
  <?php 
                endif;
?>
  <div class="row mrgnT30">
    <div class="col-md-offset-1 col-sm-offset-1  col-md-10 col-sm-10 col-xs-12">
      <div class="contact-form">
        <div class="row mrgnT30">
          <div class="col-md-offset-2 col-sm-offset-1  col-md-8 col-sm-10 col-xs-12">
            <?php 
			
	 if(isset( $_GET['act'])=='logout')
	 {
	 
	  echo "<div class=\"front_login\"><div id=\"login_success\">You have successfully logged out.</div></div>";
	 }
			
			
  if($user_ID!=0)
  {
      wp_redirect( home_url('profile'));    
	// If user is not logged in.
  }
    else{ 
	  if ( isset( $_GET['err'] )) :
	  $forget_pass='<a href="'.site_url('my-account/lost-password').'">Lost your password?</a>';
	  echo "<div class=\"front_login\"><div id=\"login_error\">ERROR: Enter valid Username and Password. $forget_pass</div></div>";
	  endif;
	// Login form arguments.
     
      $args = array(
                        'echo'           => true,
                        'redirect'       => home_url( '/dashboard/' ), 
                        'form_id'        => 'loginform',
						            'form_class'        => 'form',
                        'label_username' => __( 'Username' ),
                        'label_password' => __( 'Password' ),
                        'label_remember' => __( 'Remember me on this computer' ),
                        'label_log_in'   => __( 'Log In' ),
                        'id_username'    => 'user_login',
                        'id_password'    => 'user_pass',
						'placeholder_username'    => 'Username',
                        'placeholder_password'    => 'Password',
                        'id_remember'    => 'rememberme',
                        'id_submit'      => 'wp-submit',
						 'class_submit'      => 'submit btn',
                        'remember'       => true,
                        'value_username' => NULL,
                        'value_remember' => NULL
                    ); 
                    
                    // Calling the login form.
                  hold_wp_login_form( $args );
    }
        ?>
            
          </div>
        </div>
      </div>
      <div class="row text-center clearfix mrgnT15 mrgnB15">
          <div class="col-md-offset-1 col-sm-offset-1  col-md-10 col-sm-10 col-xs-12 login-links">
            <a href="<?php echo home_url('my-account/lost-password') ?>"> Forgot your Password ? </a> | <a href="<?php echo get_site_url();?>/register">Sign Up</a> 
          </div>
      </div>
    </div>
  </div>
  
</div>
<?php get_footer(); ?>
