<?php
/**
 * SP Theme functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage SP_Theme
 * @since SP Theme 1.0
 */

require_once('inc/ajax.php');
/**
 * SP Theme only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'sp_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own sp_theme_setup() function to override in a child theme.
 *
 * @since SP Theme 1.0
 */
function sp_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on SP Theme, use a find and replace
	 * to change 'sp_theme' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'sp_theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since SP Theme 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 60,
		'width'       => 182,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'sp_theme' ),
		'social'  => __( 'Social Links Menu', 'sp_theme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', sp_theme_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // sp_theme_setup
add_action( 'after_setup_theme', 'sp_theme_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since SP Theme 1.0
 */
function sp_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sp_theme_content_width', 840 );
}
add_action( 'after_setup_theme', 'sp_theme_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since SP Theme 1.0
 */
function sp_theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'sp_theme' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'sp_theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Home Page Search', 'sp_theme' ),
		'id'            => 'home-search',
		'description'   => __( 'Appears after menu in homepage', 'sp_theme' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Disclaimer', 'sp_theme' ),
		'id'            => 'footer-disclaimer',
		'description'   => __( 'Appears at the bottom of the content on footer.', 'sp_theme' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'sp_theme_widgets_init' );

if ( ! function_exists( 'sp_theme_fonts_url' ) ) :
/**
 * Register Google fonts for SP Theme.
 *
 * Create your own sp_theme_fonts_url() function to override in a child theme.
 *
 * @since SP Theme 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function sp_theme_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'sp_theme' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'sp_theme' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'sp_theme' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since SP Theme 1.0
 */
function sp_theme_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'sp_theme_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since SP Theme 1.0
 */
function sp_theme_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'sp_theme-fonts', sp_theme_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'sp_theme-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'sp_theme-ie', get_template_directory_uri() . '/css/ie.css', array( 'sp_theme-style' ), '20160412' );
	wp_style_add_data( 'sp_theme-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'sp_theme-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'sp_theme-style' ), '20160412' );
	wp_style_add_data( 'sp_theme-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'sp_theme-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'sp_theme-style' ), '20160412' );
	wp_style_add_data( 'sp_theme-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'sp_theme-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'sp_theme-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'sp_theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160412', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'sp_theme-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160412' );
	}

	wp_enqueue_script( 'sp_theme-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160412', true );

	wp_localize_script( 'sp_theme-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'sp_theme' ),
		'collapse' => __( 'collapse child menu', 'sp_theme' ),
	) );
}
add_action( 'wp_enqueue_scripts', 'sp_theme_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since SP Theme 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function sp_theme_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'sp_theme_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since SP Theme 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function sp_theme_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since SP Theme 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function sp_theme_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

	if ( 'page' === get_post_type() ) {
		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	} else {
		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'sp_theme_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since SP Theme 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function sp_theme_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'sp_theme_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since SP Theme 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function sp_theme_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'sp_theme_widget_tag_cloud_args' );

/**
	 * Disable readmore text
	 *
	 *@param string $more {
	 *     @string int|string   $selected              Value of the option that should be selected. Default 0.
	 * }
	 * @return boolean true/false.
*/
function new_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more');




/**
	 * Disable admin bar
	 *
	 * no arguments were needed.
	 *
	 * @return boolean true/false.
*/
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}
add_action('after_setup_theme', 'remove_admin_bar');


/**
	 * Disable calculations
	 *
	 * The `$sources` arguments were added.
	 *
	 * @param array|string $array {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $depth                 Maximum depth. Default 0.
	 *     @type int          $child_of              Page ID to retrieve child pages of. Default 0.
	 *     @type int|string   $selected              Value of the option that should be selected. Default 0.
	 * }
	 * @return boolean true/false.
*/
function meks_disable_srcset( $sources ) {
    return false;
}
 
add_filter( 'wp_calculate_image_srcset', 'meks_disable_srcset' );


/**
	 * Retrieve downloable file by name
	 *
	 * The `$array, $name` arguments were added.
	 *
	 * @param array|string $array {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $depth                 Maximum depth. Default 0.
	 *     @type int          $child_of              Page ID to retrieve child pages of. Default 0.
	 *     @type int|string   $selected              Value of the option that should be selected. Default 0.
	 * }
	 * @return string HTML content, if not displaying.
*/
function get_file_by_name($array,$name)
{
	
	foreach( $array as $key => $each_download ) 
	{
			if($each_download["name"]==$name)
			{
		 	 $file_downlaod='<a href="javascript:void(0);" class="popup_file" data-file="'.$each_download["file"].'">View </a>';
			 break;
			}
			
	}
	
	return $file_downlaod;
}

/**
	 * Retrieve downloable files
	 *
	 * The `$array, $name` arguments were added.
	 *
	 * @param array|string $array {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $depth                 Maximum depth. Default 0.
	 *     @type int          $child_of              Page ID to retrieve child pages of. Default 0.
	 *     @type int|string   $selected              Value of the option that should be selected. Default 0.
	 * }
	 * @return string HTML content, if not displaying.
*/


function download_files($array,$name)
{
	if(count($array)>0)
	{
	$file_downlaod='<h3>Documents</h3>';	
	$file_downlaod.='<ul class="list_n download_docs">';	
	foreach( $array as $key => $each_download ) 
	{
		$pdf_name =$each_download["name"]; 
		$pdf_name = str_replace('-',' ',$pdf_name);
		$pdf_name = str_replace('_',' ',$pdf_name);
		$pdf_name = str_replace('.pdf',' ',$pdf_name);
			if($each_download["name"]!=$name)
			{
		 	 $file_downlaod.=' <li><a href="javascript:void(0);" data-file="'.$each_download["file"].'" class="popup_file"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$pdf_name.' <span class="view_doc_a">View</span></a></li>';
			}
			
	}
	$file_downlaod.='</ul>';
	}
	
	return $file_downlaod;
}

/**
	 * Retrieve downloable invested files
	 *
	 * @param array|string $array {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $depth                 Maximum depth. Default 0.
	 *     @type int          $child_of              Page ID to retrieve child pages of. Default 0.
	 *     @type int|string   $selected              Value of the option that should be selected. Default 0.
	 * }
	 * @return string HTML content, if not displaying.
*/
function download_invested_files($array)
{
	if(count($array)>0)
	{
	$file_downlaod='';	
	foreach( $array as $key => $each_download ) 
	{
		
		 	 $file_downlaod.=' <li><a href="javascript:void(0);" data-file="'.$each_download["file"].'" class="popup_file"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$each_download["name"].'  <span class="view_doc_a">View</span></a></li>';
			
			
	}
	}
	
	return $file_downlaod;
}


/**
	 * Retrieve first image from downloadble files
	 *
	 * @param string $post_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 * }
	 * @return string image url.
*/
function get_product_first_img( $post_id ) {
    $args = array(
        'posts_per_page' => 1,
        'order'          => 'ASC',
        'post_mime_type' => 'image',
        'post_parent'    => $post_id,
        'post_status'    => null,
        'post_type'      => 'attachment',
    );
    $attachments = get_children( $args );
    $url= wp_get_attachment_thumb_url( $attachments[0]->ID );
	
	return $url;
    
}



/**
	 * Set session if wordpress session start fails
	 *
	 * @param none
	 * @return null.
*/
function register_sp_theme_session()
{
  if( !session_id() )
  {
    session_start();
  }
}
add_action('init', 'register_sp_theme_session');



/**
	 * retired attachement step
	 *
	 *
	 * @param string $post_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 * }
	 * @return attachemnt title.
*/

function get_product_attachments_step($post_id)
{
	  $attachments = get_post_meta( $pro_id, '_downloadable_files', true );
     if ( $attachments ) {

        foreach ( $attachments as $attachment ) {

		   
		   echo $attachment->post_title;
          }
     }

}

/**
	 * Retrieve downloable invested files
	 *
	 * @param array|string $array {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $depth                 Maximum depth. Default 0.
	 *     @type int          $child_of              Page ID to retrieve child pages of. Default 0.
	 *     @type int|string   $selected              Value of the option that should be selected. Default 0.
	 * }
	 * @return string HTML content, if not displaying.
*/
function download_files_step($array,$name)
{
	if(count($array)>0)
	{
	$file_downlaod='';	
	$file_downlaod.='<ul class="list_n download_docs">';
	$i=0;	
	foreach( $array as $key => $each_download ) 
	{
		
			if($each_download["name"]!=$name)
			{
			
			if(isset($_SESSION['invest']["download"]))
			{
				if(in_array($each_download["file"],$_SESSION['invest']["download"]))
				{
					$data_download='data-downloaded="Yes"';
					$dclass='dowloaded';
				}
				else
				{
					$data_download='data-downloaded="No"';
					$dclass='';
				}
			}
			else
			{
					$data_download='data-downloaded="No"';
					$dclass='';
			}
			
			$fil_name = ($each_download["name"]) ? $each_download["name"] : basename($each_download["file"]);
			
		$fil_name = str_replace('-',' ',$fil_name);
		$fil_name = str_replace('_',' ',$fil_name);
		$fil_name = str_replace('.pdf',' ',$fil_name);
		
		
		$file_downlaod.='<li class="file_'.$i.'"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$fil_name.'<div class="pull-right"> <a href="javascript:void(0);" 
		 '.$data_download.' class="'.$dclass.'" data-site-url="'.get_site_url().'" data-download-file="'.$each_download["file"].'" data-dwonload-name="'.$each_download["name"].'" data-download-count="'.$i.'"  ><span>View</span></a> </div>
              </li>
			 ';
			}
		$i++;	
	}
	$file_downlaod.='</ul>';
	}
	
	return $file_downlaod;
}






/**
	 * retirve attribute by id
	 *
	 *
	 * @param string $array,$name {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 *     @type string          $name                 name.
	 * }
	 * @return invested array.
*/
function get_attr_byid($arr,$name)
{
	foreach($arr as $a)
	{
		if($a['slug']==$name)
		{
			return $a['value']; 
			break;
		}
	}
}






/**
	 * retrive projects by user id
	 *
	 *
	 * @param string $user_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $user_id                 User id. Default 0.
	 *     @type array          $status                Woocommerce order status.
	 * }
	 * @return all investments as array
*/
function get_user_pojects($user_id='',$status = array('wc-pending,','wc-completed'))
{
	$user_id = ($user_id) ? $user_id : get_current_user_id();
$args = array('post_type'=>'shop_order','post_status'=>$status,'meta_query'=>array(
		array(
			'key'     => '_customer_user',
			'value'   => $user_id,
			'compare' => '=',
		),
	));
 $get_invesetments = new WP_Query($args);
 return $get_invesetments->posts;
 
}



/**
	 * fix for wordpress http error
	 *
	 *
	 * @param string $array {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $user_id                 User id. Default 0.
	 *     @type array          $status                Woocommerce order status.
	 * }
	 * @return all investments as array
*/
add_filter( 'wp_image_editors', 'change_graphic_lib' );

function change_graphic_lib($array) {
	return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}


/**
	 * retrive sub properties of a property
	 *
	 *
	 * @param int $product_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $product_id                 User id. Default 0.
	 * }
	 * @return all investments as array
*/
function sub_property_display($product_id)
{
	
	$related_properties = get_post_meta($product_id,'properties',true);
	$rel_pro_html="";
	if(!empty($related_properties)){
		
			$rel_pro_html.='<h2 class="title_group"> PROPERTIES WITHIN THIS PORTFOLIO </h2>';
	 $rel_pro_html.='<div class="row">';
     $rel_pro_html.='<div class="col-md-12 col-sm-12">';
	$rel_pro_html.='<div id="similar_properties_carousel" class="owl-carousel"> ';
		
		foreach($related_properties as $related_propertie){
			$pdf_url = (is_numeric ($related_propertie['pdf'])) ? ot_meta_attachment_by_id($related_propertie['pdf']) : $related_propertie['pdf'];
			$image_url = (is_numeric ($related_propertie['image'])) ? ot_meta_attachment_by_id($related_propertie['image']) : $related_propertie['image'];
			$pdf_url = do_shortcode($pdf_url);
			$image_url = do_shortcode($image_url);
			
			$rel_pro_html.='  
				<div class="item"> 
                  <a data-toggle="modal"  class="load_ppopup" href="#myModal" data-file="'.$image_url.'"  data-title="'.$related_propertie['title'].'"><img src="'.$image_url.'" class="img-responsive wp-post-image" alt="11" /></a>
                  <h4>'.$related_propertie['title'].' </h4>
                  <p><a data-toggle="modal"  class="load_ppopup"  href="#myModal" data-file="'.$pdf_url.'"  data-title="'.$pdf_url.'">Read More</a></p>
				  </div>';
			
		}
		
		$rel_pro_html.=' <div class="customNavigation text-center"> <a class="prev"></a> <a class="next"></a> </div>';
	$rel_pro_html.='</div>';
	$rel_pro_html.='</div>';	
	$rel_pro_html.='</div>';
	}
   return $rel_pro_html;
}



/**
	 * retrive user image url
	 *
	 *
	 * @param int $product_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $product_id                 User id. Default 0.
	 * }
	 * @return image url, if not default image will return
*/
function user_img_url($current_user_id)
{
	$img=get_the_author_meta('image',$current_user_id);
	if($img=='')
	{
		$img_url=get_template_directory_uri().'/images/icon-user-default.png';
	}
	else
	{
		$img_url=$img;
	}
	return $img_url;
	
	
}



/**
	 * check if a user is invested in a property or not
	 *
	 *
	 * @param string $user_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $user_id                 user id. Default 0.
	 *     @type int          $post_id                 post id. Default 0.
	 * }
	 * @return ok and err if fails
*/
function check_invested_prop($user_id,$prop_id)
{
	$user_id = ($user_id) ? $user_id : get_current_user_id();

 $args = array('post_type'=>'shop_order','post_status'=>array('wc-pending,','wc-completed'),'meta_query'=>array(
 	'relation'=>'AND',
		array(
			'key'     => '_invest_product_id',
			'value'   => $prop_id,
			'compare' => '=',
		),
		array(
			'key'     => '_customer_user',
			'value'   => $user_id,
			'compare' => '=',
		),
	));
 $get_invesetments = new WP_Query($args);
 return $get_invesetments->post;
}


/**
	 * check property is reserved or not
	 *
	 *
	 * @param string $post_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $post_id                 Post id. Default 0.
	 * }
	 * @return ok and err if fails
*/
function check_reserved_prop($user_id,$prop_id)
{
	global $wpdb;
$querystr = " SELECT * 
    FROM wp_reserv_spot
    WHERE wp_reserv_spot.user_id = {$user_id} and wp_reserv_spot.pro_id = {$prop_id}
    ORDER BY id DESC";
 $results = $wpdb->get_results($querystr, OBJECT);
 
 
 return $results;
}


/**
	 * retirve currnet users projects
	 *
	 *
	 * @param string $user_id {
	 *     Optional. Array or string of arguments to generate list of downloaddable files.
	 *
	 *     @type int          $user_id                 User id. Default 0.
	 * }
	 * @return ok and err if fails
*/
function get_current_pros($user_id)
{
	$html="";
 $args = array(
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    'terms' => "current-opportunities"
                )
            ),
            'post_type' => 'product',
            'orderby' => 'title,'
        );	
	$loop = new WP_Query( $args );
	$current_opportunities_products = array();
 	 while ( $loop->have_posts() ) : $loop->the_post(); 
 	 
 	 global $product;
 	 $pro_id=$loop->post->ID;
			$proa = get_product($pro_id);
			
			$check_pro=check_invested_prop($user_id,$pro_id);
			
			if(count($check_pro)==0)
			{
				$current_opportunities_products[] = $proa;
			}
 	 	
		endwhile;
            
			
			if(!empty($current_opportunities_products)){
				$html .= '<div class="bg-white mrgnB15 current_oppor_slider">
        <h3 class="sub_title" 8=""> <img src="'. get_template_directory_uri().'/images/professional-advance.png" alt="">Current Opportunities</h3>
		
		
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" role="listbox">';
				foreach($current_opportunities_products as $pro){
					
			
			
			$src = $pro->get_image('default',array("class"=>"img-big-width"));
			$src_past = $pro->get_image('default',array("class"=>"img-responsive"));
			$address = $pro->get_attribute( 'address' );
			
			
			$minimum_investment = $pro->get_attribute( 'minimum-investment' );
			$irr_from = $product->get_attribute( 'irr-from' );
			$irr_to = $product->get_attribute( 'irr-to' );
			$no_properties= $product->get_attribute( 'no-of-properties' );
		    $property_type= $product->get_attribute( 'property-type' ); 
			$property_term = $pro->get_attribute( 'property-term' );
			$estimated_market_value = $pro->get_attribute( 'estimated-fair-market-value' );
			$detail_url = get_permalink( $pro_id );
			$project_name=get_the_title($pro_id);
			
			
			// output as html
			$html.='<div class="item">
					<div class="inv_item_block">
					<div class="image_block">
						'.$src_past.'
						<div class="img_content_blk">
							<label>'.$address.'</label>
							<span>'.$project_name.'</span>
						</div>
					</div>
					<!-- // image_block -->
					
					<div class="invest_brif">
						<div class="row">
							<div class="col-md-3 col-sm-3 col-xs-12 brder_r">
								<label>IRR</label>
								<span class="label_value">'.$irr_from.'% - '.$irr_to.'%</span>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12 brder_r">
								<label>Min. Investment</label>
								<span class="label_value">$'.$minimum_investment.'</span>
							</div>
							<div class="col-md-5 col-sm-5 col-xs-12 brder_r">
								<label>Cash on Cash Return</label>
								<span class="label_value">10% - 12%</span>
							</div>
						</div>
						<!--// end row -->
					</div>
					</div>
					</div>';
		
				}
				
				$html .= '
	  </div>
		<div class="slider-control">
			 <!-- Controls -->
		  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<i class="fa fa-angle-left" aria-hidden="true"></i>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<i class="fa fa-angle-right" aria-hidden="true"></i>
		  </a>
		</div>
		</div>';
			}
				
				
	
 
 return $html;
}
